% Author:          Tony Hammack
% Date:            6 October 2017
% Script Name:     P1_1_6
% Purpose:         Displays the perimeter of an ellipse

clc
a = input('Please enter a value for (a): ');
b = input('Please enter a value for (b): ');
% Check to mkake sure a or b are not zero. Otherwise the equation of the 
%   ellipse will not make sense. 
check = 0; 
while check == 0
    if (a == 0) || (b == 0)
    clc
        disp('You must enter values that are non-zero.')
        a = input('Please enter a value for (a): ');
        b = input('Please enter a value for (b): ');
    else
        check = 1;
    end
end
clc
% Definitions of the formulas
h = ((a-b)/(a+b))^2;
p_1 = pi*(a+b);
p_2 = pi*sqrt(2*(a^2+b^2));
p_3 = pi*sqrt(2*(a^2+b^2)-((a-b)^2)/2);
p_4 = pi*(a+b)*(1+(h/8))^2;
p_5 = pi*(a+b)*(1+((3*h)/(10+sqrt(4-3*h))));
p_6 = pi*(a+b)*(64-3*h^2)/(64-16*h);
p_7 = pi*(a+b)*(256-48*h-21*h^2)/(256-112*h+3*h^2);
p_8 = pi*(a+b)*(3-sqrt(1-h))/2;
mean = (p_1+p_2+p_3+p_4+p_5+p_6+p_7+p_8)/8;
% Display output
fprintf('a = %2d, b = %3.3, h = %3.6f\n\n',a,b,h)
disp('---------------------------------')
fprintf('\nPerimeters of the ellipse with different approximation formulas.\n')
fprintf('P_1 = %3.6f\n',p_1) 
fprintf('P_2 = %3.6f\n',p_2) 
fprintf('P_3 = %3.6f\n',p_3) 
fprintf('P_4 = %3.6f\n',p_4) 
fprintf('P_5 = %3.6f\n',p_5) 
fprintf('P_6 = %3.6f\n',p_6) 
fprintf('P_7 = %3.6f\n',p_7) 
fprintf('P_8 = %3.6f\n',p_8)
fprintf('The mean perimeter is: %3.6f.\n',mean) 




