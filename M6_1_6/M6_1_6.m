% Author:          Tony Hammack
% Date:            22 October 2017
% Script Name:     M6_1_6
% Purpose:         Plots Bullseyes for uniformly and aimed dart throws. 

clc
% Initializations
n = 10000; 
rand('seed',0); 
randn('seed', 0);

% Throw uniform darts
u_x = -1+2*rand(n,1); u_y = -1+2*rand(n,1);

% Thow darts with aiming....
sigma = 0.4; 
a = sigma*randn(n,1); b = sigma*randn(n,1);
a_x = []; a_y = [];
for k=1:n
    %Check if kth dart thrown inside box
    if abs(a(k)) <=1 && abs(b(k)) <= 1
        a_x(end + 1) = a(k);
        a_y(end + 1) = b(k);
    end
end

% Graph Equation
graphBullseye(n,u_x,u_y,a_x,a_y)





