% Author:          Tony Hammack
% Date:            24 October 2017
% Purpose:         Graph Bullseyes

function graphBullseye(n,u_x,u_y,a_x,a_y)
    close all
    
    msize = 3;  % Marker Size
    % Define bounndaries of the box
    bx = [-1 -1 1 1];
    by = [-1 1 1 -1];
    gray = [0.7 0.7 0.7]; 
    % Define Circle
    theta = linspace(0,2*pi,n);
    x = cos(theta);
    y = sin(theta);
    white = [1,1,1];
    
    % plot bullseye via uniform 
    figure(1)
    axis equal off
    hold on
    plot(u_x,u_y,'.','MarkerSize',msize)
    hold off
    hold on
    plot(bx,by,'k-') % Black Box outline
    hold off
    hold on
    fill(bx,by,gray) % Coloured Box
    hold off
    hold on
    plot(x,y,'k') % outline of circle
    hold off
    hold on
    fill(x,y,white) % white filled circle
    hold off
    title(['Darts Thrown Uniformly'])

    
     % plot bullseye via aim 
    figure(2)
    axis equal off
    hold on
    plot(a_x,a_y,'.','MarkerSize',msize)
    hold off
    hold on
    plot(bx,by,'k-') % Black Box outline
    hold off
    hold on
    fill(bx,by,gray) % Coloured Box
    hold off
    hold on
    plot(x,y,'k') % outline of circle
    hold off
    hold on
    fill(x,y,white) % white filled circle
    hold off
    title(['Darts Thrown with Aim'])
end
