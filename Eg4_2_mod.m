% Author:          Tony Hammack
% Date:            2 October 2017
% Script Name:     Eg4_2_mod
% Purpose:         Displays interpolants of the colours red & green, red
%   & blue, and green & blue.

close all
 
% Initialisations
red = [1 0 0];     % rgb of red
green = [0 1 0];   % rgb of green
blue = [0 0 1];    % rgb of blue
n = 10;            % the number of 'in between' coplours is n-1
x = [0 3 3 0];     % locates the x-values in the tiles
y = [0 0 1 1];     % locates the y-values in the tiles

% Add coloured tiles to the figure window (red to green)...
figure(1)
axis equal off
hold on
for j=0:n
    % Display the jth tile and its rgb value...
    f=j/n;
    v1 = (1-f)*red + f*green;
    fill(x,y+j,v1)
    text(3.5,j+.5,sprintf('[ %4.2f, %4.2f, %4.2f ]', v1(1),v1(2), v1(3)))
end
hold off

% Add coloured tiles to the figure window (red to blue)...
figure(2)
axis equal off
hold on
for k=0:n
     % Display the kth tile and its rgb value...
     f2=k/n;
     v2 = (1-f2)*red + (f2)*blue;
     fill(x,y+k,v2)
     text(3.5,k+.5,sprintf('[ %4.2f, %4.2f, %4.2f ]', v2(1),v2(2), v2(3)))
 end
hold off

% Add coloured tiles to the figure window (green to blue)...
figure(3)
axis equal off
hold on
for l=0:n
    % Display the lth tile and its rgb value...
    f3=l/n;
    v3 = (1-f3)*green + f3*blue;
    fill(x,y+l,v3)
    text(3.5,l+.5,sprintf('[ %4.2f, %4.2f, %4.2f ]', v3(1),v3(2), v3(3)))
end
hold off

shg

 