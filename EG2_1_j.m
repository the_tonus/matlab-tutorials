% Author:  Tony Hammack
% Date:    12 September 2017
% Script:  Eg2_1_j
% Purpose: Pi Via Tiling (M2.1.4)

% Enter the Disk Radius...
clc
n = input('Enter an integral radius n: ');
clc
for j=1:8 
    n_0 = n^j;
    %Uncut Tile Method
    % Tile just the first quadrant, then multiply by four (symmetry)...
    N1 = 0; 
    for k=1:n_0
        % Add in the number of uncut tiles in row k
        m = floor(sqrt(n_0^2 - k^2));
        N1 = N1 + m;
    end
    
    % Display the estimate...
    rho_n = (4*N1)/(n_0^2); %Obtained from uncut tiles
    fprintf('j         = %1d\n',j)
    fprintf('n         = %1d\n',n_0)
    fprintf('rho_n     = %12.8f\n',rho_n)
    fprintf('Error     = %12.8f\n',abs(pi-rho_n)) % pi vs rho
    fprintf('\n')
end



