function relativeErr(t,u,x) % Relative Error of APprox and True
    err = abs(t-u)/t;
    fprintf('%7e    %3.7e\n',x,err)
end