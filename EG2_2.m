% Author:          Tony Hammack
% Date:            12 September 2017
% Script Name:     EG2_2
% Purpose:         Pie Via Polygons

% Input the iteration parameters....
clc
delta = input('Enter the error tolerance: ');
nMax = input('Enter the iteration bound: ');

% The Triangle Case...
n = 3;                       % Number of polygon edges
A_n = (n/2)*sin(2*pi/n);     % Inscribed Area
B_n = (n)*tan(pi/n);         % Circumscribed Area

% Initialise the ErrorBounds so that they are greater than delta.
ErrorBound_A = delta + 1;
ErrorBound_B = delta + 1;

% Iterate while error too big...
while (ErrorBound_A > delta || ErrorBound_B > delta)
        n = n + 1;
        A_n1 = (n/2)*sin(2*pi/n);
        B_n1 = (n)*tan(pi/n);
    
        % Calculates Error Bounds for A_n and B_n
        ErrorBound_A = abs(A_n1 - A_n);
        ErrorBound_B = abs(B_n1 - B_n);
    
        A_n = A_n1;
        B_n = B_n1;    
end

% Display the final approximation....
rho_nStar = (A_n + B_n)/2;
clc 
fprintf('delta = %10.3e\nnStar = %1d\nErrorBound_A = %20.15f\nErrorBound_B = %20.15f\n\n', delta, nStar, ErrorBound_A, ErrorBound_B)
fprintf('rho_nStar    = %20.15f\nPi           = %20.15f\n', rho_nStar, pi)
