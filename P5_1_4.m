% Author:          Tony Hammack
% Date:            19 October 2017
% Script Name:     P5_1_4
% Purpose:         Returns relative error of sin(x) and f(x)=x-(x^3)/6

clc 
disp('     x          Relative Error')
disp('------------------------------')
for k=-20:1
    x = 2^k;
    t = sin(x);  % True value
    u = ApproxSine(x); % Approximated Value
    
    % Finds Relative Error and Prints Results
    relativeErr(t,u,x)
end

% Functions
function u = ApproxSine(x)  % Approximation
    u = x-(x^3)/6;
end
