% Author:          Tony Hammack
% Date:            1 November 2017
% Script Name:     P5_2_7
% Purpose:         Check Poyline Function with a regular n-gon inscribed 
%   in a unit circle

clc
format long
n = input('Enter number of vertices: ');    % Input
clc

TheThetas = linspace(0,2*pi,n+1);
c = cos(TheThetas); s = sin(TheThetas); % Creates vertices of n-gon1000
% Since u(1) = u(n+1) and v(1) = v(n+1)
c(end + 1) = c(1); s(end + 1) = s(1);   
L = PolyLine(c,s);

fprintf(' The value of the perimeter is: %3.15f\n',L) % Output

