% Author:          Tony Hammack
% Date:            6 October 2017
% Script Name:     P3_1_7
% Purpose:         Displays the graphic with asterisks

clc
n = input('Enter the number of asterisks for each side of the square: ');
check = 0;
while check == 0
    if n <= 3
        clc
        disp('The value of n is required to be greater than 3.')
        n = input('Enter the number of asterisks for each side of the square: ');
    else
        check = 1;
    end
end
clc

% Draw plot

% Plots top row
for i=1:n
    fprintf('*')
end
fprintf('\n')
% Plots the diagonal
for r = 2:n-1
    for j=1:n
        if j==r
            fprintf('*')
        elseif j == 1
            fprintf('*')
        elseif j == n
            fprintf('*')
        else
            fprintf(' ')
        end
    end
    fprintf('\n')
end
% Plots the bottom row
for l=1:n
    fprintf('*')
end
fprintf('\n')
