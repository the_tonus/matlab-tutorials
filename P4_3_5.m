% Author:          Tony Hammack
% Date:            1 November 2017
% Script Name:     P4_3_5
% Purpose:         Plot a function with and w/o Horner's scheme

clc
close all

% Calculate x
points = 99; %100 - 1 points, if not, you will have 101 points
steps = abs(0.995-1.005)/points;
x = 0.995:steps:1.005;

% Initialize f(x) and g(x)
y_h = zeros(1,100); y_g = zeros(1,100); 
n = 6;     % Degree of Polynomial

% Function Evaluation w/o Horner's scheme
y_g(1:100) = x.^6 - 6.*x.^5 + 15.*x.^4 - 20.*x.^3 + 15.*x.^2 - 6.*x + 1;

% Function Evaluation with Horner's scheme
 a = [1 -6 15 -20 15 -6 1]; % Coefficients of the polynomial
for i = 1:100
    p = zeros(1,n+1);
    p(n+1)=a(n+1);
    for j = n:-1:1
        p(j)=(p(j+1)*x(i)) + a(j);
    end
    y_h(i) = p(1);
end

figure
plot(x,y_h,'r',x,y_g,'b')
legend('With Horners Scheme', 'Without Horners Scheme')
title('P4.3.5')
shg


