% Author:          Tony Hammack
% Date:            1 November 2017
% Script Name:     EG7_1
% Purpose:         Script that checks the accuracy of function stationary.m
%   with predetermined transition probability matrix

clc
% Sets up the transition probability matrix and initialize the state vector
P = [ .32 .17 .11 .46; ...
      .18 .43 .32 .33; ...
      .27 .22 .39 .14; ...
      .23 .18 .18 .07 ];
x = (10^6)*ones(4,1); % Initial state vector
tol = 1; % Desired Tolerance
itMax = 10^3; % Maximum time step interations

% Simiulate the 5 time steps and display
clc
disp('      x(1)        x(2)         x(3)         x(4)     ')
disp('-----------------------------------------------------')
disp(sprintf(' %8.0f     ',x))
stationary(P,x,tol,itMax);

