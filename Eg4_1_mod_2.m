% Author:          Tony Hammack
% Date:            22 September 2017
% Script Name:     Eg4_1_mod_2
% Purpose:         Plots f(x)=sin(5x)*exp(x/2)/(1+x^2) and finds local
%   extrema

clc
L = -2;  % Left Endpoint
R = 3;   % Right Endpoint
N = 200; % Number of Sample Points

% Obtain the vector of x-values and f-values
x = linspace(L,R,N);
F = @(x) (sin(5*x) .* exp(-x/2)) ./ (1+x.^2);
y=F(x);

% Find the zeros and local extrema of f(x)
% ---------------------------------
% Find the zeros of the funciton
zeros = [];
for i=1:numel(x)-1
    try
        zeros(end+1) = fzero(F,[x(i),x(i+1)]);
    end
end 
% Print the zeros of the function.
fprintf('--------------------------\n')
disp('The zeros of f(x) are: (x)')
disp('--------------------------')
for j=1:length(zeros)
    fprintf('%1.4f\n',zeros(j))
end

% Find Local Maxima
[maxima_pks,maxima_locs] = findpeaks(y);
% Print the zeros of the function.
fprintf('\n\n-------------------------------------\n')
disp('The local maxima of f(x) are: (x,f(x)')
disp('-------------------------------------')
for j=1:length(maxima_locs)
    fprintf('%1.4f, %1.4f\n',x(maxima_locs(j)),maxima_pks(j))
end

% Find Local Minima
[minima_pks,minima_locs] = findpeaks(-y);
% Print the local minima of the function.
fprintf('\n\n--------------------------------------\n')
disp('The local minima of f(x) are: (x,f(x))')
disp('--------------------------------------')
for k=1:length(minima_locs)
    fprintf('%1.4f, %1.4f\n',x(minima_locs(k)), -minima_pks(k))
end
%------------------------

% Plot and label....
plot(x,y, ...
[L R],[0 0],':', ...
zeros,0,'xg', ...
x(minima_locs), -minima_pks, 'ob',...
x(maxima_locs), maxima_pks, 'or')
title('The function f(x) = (sin(5x) * exp(x/2)) / (1+x^2)')
ylabel('y = f(x)')
xlabel('x')