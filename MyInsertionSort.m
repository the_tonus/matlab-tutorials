% Author:          Tony Hammack
% Date:            16 November 2017
% Purpose:         Add idx to InsertionSort function so that y = x(idx)

function [y,TotalC,TotalS,idx] = MyInsertionSort(x)
    % x is a column n-vector
    % y is a column n-vector obtained by permuting the values in x
    %     so that they are ordered from smallest to largest.
    % TotalC is the total number of required comparisons. 
    % TotalS is the total number of required swaps.
    % idx is a row n-vector obtained by permuting the values 
    %     of the indicies of x
    n = length(x); TotalC = 0; TotalS = 0;
    idx = 1:1:n;
    for k=2:n
        [x(1:k),C,S,idx] = Insert(x(1:k),idx);
        TotalC = TotalC + C;
        TotalS = TotalS + S;
    end
    y=x;
end

function [y,C,S,idx] = Insert(x,idx)
    % x is a column m-vector with x(1:m-1) sorted.
    % y is a column m-vector obtained by applying
    %   the insert process
    % C is the number of required comparisons. 
    % S is the number of required swaps.
    % idx is a row m-vector that records the indices of
    %   of the permuted x
    m = length(x); S = 0;
    k = m - 1;
    while k>= 1 && x(k)>x(k+1)
        t = x(k+1); x(k+1) = x(k); x(k) = t; S = S+1; % Sorts x
        t_idx = idx(k+1); idx(k+1) = idx(k); idx(k) = t_idx; % Records indices of x
        k = k-1;
    end
    y=x;
    C=S+1;
end
