% Author:          Tony Hammack
% Date:            2 October 2017
% Script Name:     P4_2_3
% Purpose:         Draw an n-gon with a red perimeter and yellow interior

clc
close all
n = input('Please enter an n: ');
% Initializations
x = [];
y = [];
yellow = [1 1 0];
% Creating the x,y vectors
for k=1:n
    x_k = cos(2*pi*k/n);
    y_k = sin(2*pi*k/n);
    x(end+1) = x_k;
    y(end+1) = y_k;
end    
% Adding the begging vertex to the end to close the form
x(end+1) = x(1);
y(end+1) = y(1);
% Plot
figure
plot(x,y,'r', 'LineWidth', 5)
hold on
fill(x,y,yellow)
title(sprintf('n = %3d',n))
