% Author:          Tony Hammack
% Date:            31 October 2017
% Function Name:   MyAverage
% Purpose:         Vectorizes Average function

function B = MyAverage(A)
% A is an mxn matrix
% B is an mxn matrix
% The first and last row and columns of A and B are the same. 
% Otherwise, B(i,j) is the average of A(i,j)'s four neighbours, 
%   A(i+1,j),A(i-1,j),A(i,j+1), and A(i,j-1).

% Initialise Matrix B
[m,n] = size(A);
B = zeros(m,n);
d = m - n; % Calculate the difference between the dim(B), this is for

% Vectorized Average function
B(2:n-1+d,2:n-1) = (( A(1:n-2+d,2:n-1) + A(3:n+d,2:n-1) + A(2:n-1+d,1:n-2) + A(2:n-1+d,3:n)) / 4 );

end
