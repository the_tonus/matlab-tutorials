% Author:          Tony Hammack
% Date:            22 September 2017
% Script Name:     Eg4_1_mod_3
% Purpose:         Plots f(x)=sin(5x)*exp(x/2)/(1+x^2)

clc
L = input('Please enter the left endpoint: ');  % Left Endpoint
R = input('Please enter the right endpoint: ');   % Right Endpoint
c = (2*L+R)/3;
d = (2*R+L)/3;
N = 200; % Number of Sample Points

% Obtain the vector of x-values and f-values
x = linspace(L,R,N);
F = @(x) (sin(5*x) .* exp(-x/2)) ./ (1+x.^2);
y=F(x);

% Creates x,y vectors 1/3 the size of x,y for the plot
x1 = [L:0.001:c] ;
y1 = F(x1);  
x2 = [c:0.001:d] ;
y2 = F(x2);  
x3 = [d:0.001:R] ;
y3 = F(x3);

% Plot and label....
plot([L R],[0 0],':', ...
x1, y1,'r--', ...     
x2, y2, 'k-',...
x3, y3, 'r--')
title('The function f(x) = (sin(5x) * exp(x/2)) / (1+x^2)')
ylabel('y = f(x)')
xlabel('x')