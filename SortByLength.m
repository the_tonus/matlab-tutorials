% Author:          Tony Hammack
% Date:            21 November 2017
% Purpose:         Reorder cell array via length of elements of cell.

function A = SortByLength(C)
    m = length(C);
    A  = cell(m,1);
    for k=2:m
        % Applies the insert subfunction to rearrange the cell
        [C(1:k)] = Insert(C(1:k));
    end
    A = C;
end

function [Y] = Insert(D)
    % D is a 1 by m cell array with D(1:m-1) sorted.
    % Y is a 1 by m cell array obtained by applying
    %   the insert process
    m = length(D);
    k = m - 1;
    while k>= 1 && length(D{k})>length(D{k+1})
        t = D(k+1); D(k+1) = D(k); D(k) = t;% Sorts x
        k = k-1;
    end
    Y = D;
end
