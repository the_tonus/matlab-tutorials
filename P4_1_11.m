% Author:          Tony Hammack
% Date:            6 October 2017
% Script Name:     P4_1_11

clc
% Initialisations
n = input('Please enter an integer in the interval [1,10]: ');
x = [1:1:10];
y = [1:1:10];
% Check to make sure n is in the interval [1,n] as specified
check = 0;
while check == 0
    if (n >= 1) && (n <= 10)
        check = 1;
    else
        clc
        n = input('Please enter an integer in the interval [1,10]: ');
    end
end 
clc
% calculate f(k) according to formula
for k=1:10
    if k == 1
        y(k) = n;
    elseif mod(y(k-1), 2) == 1 % f(k-1) is odd
        y(k) = 3*(y(k-1))+1;
    elseif mod(y(k-1), 2) == 0 % f(k-1) is even
        y(k) = y(k-1)/2;
    end
end
% Display output
fprintf('The points of f(k).\n')
for i = 1:10
    fprintf('(%2d, %2d)\n', x(i),y(i))
end
% plot graph
plot(x,y,'*k')
