% Author:          Tony Hammack
% Date:            22 October 2017
% Script Name:     Eg6_2_mod
% Purpose:         Finds the coordinates of a random walk in 8 directions 
%  and estimates the average number of hops required to reach the boundary. 

clc
disp('   n   Average to Boundary')
disp('--------------------------')
nTrials = 100;
for n=5:5:50
    s = 0;
    for k=1:nTrials     % Finds the x,y coordinates
        [x,y] = RandomWalk2D(n);
        s = s + length(x);
    end
    ave = s/nTrials;
    fprintf(' %3d        %8.3f\n',n,ave)
end
fprintf('\n\n(Results based on %d trials)\n',nTrials)