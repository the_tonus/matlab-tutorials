% Author:          Tony Hammack
% Date:            22 October 2017
% Purpose:         Find x,y coordinates of random walk

function [x,y] = RandomWalk2D(n)
    % Initialise Counter and location at origin
    k=0; xc = 0; yc = 0;
    % With 8 possible directions, we partition the probability space into
    %       8 distinct partitions
    while abs(xc)<n && abs(yc)<n
        % the random walk
        r = rand(1);
        if r <= 0.125       % Hop North
            yc = yc + 1;
        elseif r <= 0.25    % Hop North East
            yc = yc + 1;
            xc = xc + 1;
        elseif r <= 0.375   % Hop North West
            yc = yc + 1;
            xc = xc - 1;
        elseif r <= 0.5     % Hop West 
            xc = xc - 1;
        elseif r <= 0.625   % Hop East 
            xc = xc + 1;
        elseif r <= 0.75    % Hop South 
            yc = yc - 1;
        elseif r <= 0.875   % Hop South East
            yc = yc - 1; 
            xc = xc + 1;
        elseif r > 0.875    % Hop South West
            yc = yc - 1;
            xc = xc - 1;
        end
        k=k+1; x(k) = xc; y(k) = yc;    % Save new location
    end
end
            