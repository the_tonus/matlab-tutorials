% Author:          Tony Hammack
% Date:            19 October 2017
% Script Name:     P5_2_5
% Purpose:         Drawing inner and outer polygons around the ellipse

clc
close all
axis equal off
figure

% Initialisations
a = input('Please enter value for a: ');
b = input('Please enter value for b: ');
n = 6;
theta = 2*pi/n;
gamma = (1-cos(theta))/sin(theta);

% Pre-Compute the thetas
TheThetas = linspace(0,2*pi,n+1);
c = cos(TheThetas);
s = sin(TheThetas);

% Find the x,y coordinates of the inner hexagon
p_x = [a*c, a*c(1)];
p_y = [b*s, b*s(1)];

% Find the x,y coordinates of the outer hexagon
o_x = [a*(c-gamma*s), a*(c(1)-gamma*s(1))];
o_y = [b*(s+gamma*c), b*(s(1)+gamma*c(1))];

% Plot Figure
ellipse_thetas = linspace(0,2*pi,10^2); % Used to plot ellipse with great resolution
h = plot(p_x,p_y, 'ro-',... % Inner Hexagon
    o_x,o_y, 'bo-',...      % Outer Hexagon
    [a*cos(ellipse_thetas), a*cos(ellipse_thetas(1))], [b*sin(ellipse_thetas), b*sin(ellipse_thetas(1))],'k-'); %Actual Ellipse
set(h,{'LineWidth'},{1;1;2.2})