% Author:  Tony Hammack
% Date:    12 September 2017
% Script:  Eg2_1
% Purpose: Pi Via Tiling

% Enter the Disk Radius...
clc
n = input('Enter an integral radius n: ');

%Uncut Tile Method
% Tile just the first quadrant, then multiply by four (symmetry)...
N1 = 0; 
for k=1:n
    % Add in the number of uncut tiles in row k
    m = floor(sqrt(n^2 - k^2));
    N1 = N1 + m;
end

%Total Tile Method
% Tile just the first quadrant, then multiply by four (symmetry)...
N2 = 0; 
for k=1:n
    % Add in the number of tiles in row k
    a = ceil(sqrt(n^2 - k^2));
    N2 = N2 + a;
end

% Display the estimate...
rho_n = (4*N1)/(n^2); %Obtained from uncut tiles
mu_n = (4*N2)/(n^2); %Obtained from total tiles
clc
fprintf('n         = %1d\n',n)
fprintf('rho_n     = %12.8f\n',rho_n)
fprintf('mu_n      = %12.8f\n',mu_n)
fprintf('Error     = %12.8f\n',abs(pi-rho_n)) % pi vs rho
fprintf('Mu vs Rho = %12.8f\n',abs(mu_n-rho_n)) % rho vs mu