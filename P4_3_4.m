% Author:          Tony Hammack
% Date:            1 November 2017
% Script Name:     P4_3_4
% Purpose:         Plot two forms of a function 

clc
close all

% Calculate x
points = 99; %100 - 1 points, if not, you will have 101 points
steps = abs(0.995-1.005)/points;
x = 0.995:steps:1.005;

% Calculate y via vectorization
y_f = zeros(1,100); y_g = zeros(1,100); % Initialize f(x) and g(x)
% f(x)
y_f(1:100) = (1-x(1:100)).^6; 
% g(x)
y_g(1:100) = x.^6 - 6.*x.^5 + 15.*x.^4 - 20.*x.^3 + 15.*x.^2 - 6.*x + 1; 

% Plot
figure
plot(x,y_f,'g',x,y_g,'b')
legend('Normal Function', 'Function Expanded')
title('P4.3.4')
shg
    