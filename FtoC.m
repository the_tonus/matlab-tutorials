% Author:  Tony Hammack
% Date:    9 September 2017
% Script:  FtoC
% Purpose: Convert Fahrenheit to Celsius

% Input Fahrenheit Temperture
f = input('Please enter the temperture in Fahrenheit: ');
clc

% Converts Fahrenheit to Celsius
c = (5*(f-32))/9;

% Output in Celsius
fprintf('The temperture in Celsius is: %10.3f\n', c)


