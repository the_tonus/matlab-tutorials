% Author:          Tony Hammack
% Date:            21 November 2017
% Purpose:         Remove spaces from string.

function s = Compress(t)
    % Initializations
    b = blanks(1); % Blank space
    s = []; 
    n = length(t);
    temp = [t]; % Create character array of t
    % Compares each entry of character array to see if it is not a space
    for i=1:n
        if temp(i) ~= b
            s = [s temp(i)];
        end
    end
end
             