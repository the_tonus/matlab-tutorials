% Author:          Tony Hammack
% Date:            16 November 2017
% Purpose:         Compute a vector that is a left shift of the other

function y = LeftShift(x)
% x and y are row n-vectors where y is a left shift of x
n = length(x);
y = zeros(n,1); % row n-vector
% The magic of the left shift that is non-iterative
y(n) = x(1);
y(1:n-1) = x(2:n);
end