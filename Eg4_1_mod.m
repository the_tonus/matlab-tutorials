% Author:          Tony Hammack
% Date:            22 September 2017
% Script Name:     Eg4_1_mod
% Purpose:         Plots f(x)=sin(5x)*exp(x/2)/(1+x^2)

L = -2;  % Left Endpoint
R = 3;   % Right Endpoint
N = 200; % Number of Sample Points

% Obtain the vector of x-values and f-values
x1 = linspace(L,R,N);
y = (sin(5*x1) .* exp(-x1/2)) ./ (1+x1.^2);

% To obtain the zeros of y
%x0 = [L R];

    
% Plot and label....
plot(x1,y,[L R],[0 0],':')
title('The function f(x) = sin(5x) * exp(x/2) / (1+x^2)')
ylabel('y = f(x)')
xlabel('x')