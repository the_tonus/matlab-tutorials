% Author:          Tony Hammack
% Date:            16 November 2017
% Purpose:         Compute a vector that is a multiple left shifts of the other

function y = MultipleLeftShift(x,k)
% x is a row n-vector and k is an integer that satisfies 1<=k<n
% y is a row n-vector obtained by left-shiting n-times
n = length(x);
y = zeros(n,1); % row vector y
% The magic of the non-iterative multiple left shits
y(1:n-k) = x(k+1:n);
y(n-k+1:n) = x(1:k);
end