% Author:          Tony Hammack
% Date:            6 October 2017
% Script Name:     P2_1_5
% Purpose:         Displays the estimates for rho, tau, and mu

clc
% Create the beginning of the table
disp('    n       |pi-rho_n|        |pi-tau_n|          |pi-mu_n|   ')
disp('--------------------------------------------------------------')
for i=1:10
   % Initialise variables
   r_n = 0;
   t_n = 0;
   u_n = 0;
   n = 100*i; % Raise n by i power
   for k = 1:n  % Calculate r_n, t_n, and u_n.
       r_n = r_n + ((-1)^(k+1))/(2*k-1);
       t_n = t_n + 1/(k^2);
       u_n = u_n + 1/(k^4);
   end
   % Create the estimates
   rho_n = 4*r_n;
   tau_n = sqrt(6*t_n);
   mu_n = nthroot(90*u_n,4);
   % Find the error of the estimates
   err_rho = abs(rho_n-pi);
   err_tau = abs(tau_n-pi);
   err_mu = abs(mu_n-pi);
   % Displays output in table
   fprintf('  %4d    %1.12f    %1.12f      %1.12f\n',n,err_rho,...
       err_tau,err_mu)
end    