% Author:          Tony Hammack
% Date:            1 November 2017
% Function Name:   PolyLine
% Purpose:         Calculates the Perimeter of a regula n-gon inscribed in 
%   a unit circle via vectorization

function L = PolyLine(u,v)
% u and v are column (n+1)-vectors with u(1) = u(n+1) and v(1) = v(n+1).
% L is the perimeter of the polygon with vertices
%       (u(1),v(1)),...,(u(n),v(n))

n = length(u) - 1; % Same process as v since u,v are n+1 column vectors
% Use 2-norm for Euclidean Distance
L = sum(sqrt( ((u(1:n)-u(2:n+1)).^2 + ((v(1:n)-v(2:n+1)).^2 ))));
end