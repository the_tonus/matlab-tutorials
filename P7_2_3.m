% Author:          Tony Hammack
% Date:            31 October 2017
% Script Name:     P7_2_3
% Purpose:         Plot the temperature of a line segement connecting (0,0) to (6,4)

% Create the temperature values of the plate.
a = 0; b = 7.211; n = 301; x = linspace(a,b,n);
TY = fOnGrid(x,[1 2 3 4],@T_plate);

% Plot the temperature of the plate from (0,0) to (6,4)
close all
figure
plot(x,TY(4,:))
xlabel('x','Fontsize',14)
ylabel('Temperature','Fontsize',14)
legend('y = 4')
axis([0 7.211 0 80])        % Defines the x,y axis to include the endpoint 7.211
set(gca,'xTick', 0:1:7.211) % Autogenerates the x-axis tick marks with a spacing of 1 unit
shg

% Check the values of the graph at (0,0) and (6,4)
 disp('Temperature at origin')
 fprintf('\n%3.4f\n\n',T_plate(0,0))   
 disp('--------')
 disp('Temperature at (6,4) ')
 fprintf('\n%3.4f\n\n',T_plate(6,4))