function stationary(P,x,tol,itMax) % Finds stationary vector
    % Initialise vectors and counter
    y0 = x; y = Transition(P,x);
    m = 1;
    while m <= itMax
        if sum(abs(y0-y)) > tol
            disp(sprintf(' %8.0f     ',y)) % Displays the mth state vector
            % Prepares for new state vector at the next time step m
            y0 = y;
            y = Transition(P,y0);
        else
        % Stops the function when we have achieved the desired tolerance
            break 
        end 
        m = m + 1; 
    end
end

% -----------------------

function y = Transition(P,x) % Creates new state vectors
    % Initializations
    [n,n] = size(P);
    y = zeros(n,1);
    for i=1:n
        % Compute the new ith state
        for j=1:n
            % Add in the contribution from the current jth state
            y(i) = y(i) + P(i,j)*x(j);
        end
    end   
end
