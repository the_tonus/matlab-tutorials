% Author:  Tony Hammack
% Date:    9 September 2017
% Script:  Eg1_1
% Purpose: Surface Area Increase

% Aquire and display the input data...
r = input('Enter radius (kilometres): ');
delta_r = input('Enter increase (millimetres): ');
clc

% Outputs in the inputs in fixed point form
fprintf('Sphere radius   = %12.6f kilometres\n', r)
fprintf('Radius increase = %12.6f millimetres\n', delta_r)

% Calculates the original surface area 
SA = 4*pi*(r^2)*(10^6);
fprintf('\nSurface Area Before Increase: %15.6f square metres\n\n', SA)

% Calculates the area increase via methods 1, 2, & 3
disp('Surface Area Increase:')

dr = delta_r/(10^6);

% Method 1
delta_A1 =  (4*pi*((r+dr)^2)- 4*pi*(r^2))*(10^6);
fprintf('\n  Method 1: %15.6f square metres\n', delta_A1)

% Method 2
delta_A2 =  (4*pi*((2*r+dr)*dr))*(10^6);
fprintf('  Method 2: %15.6f square metres\n', delta_A2)

% Method 3
delta_A3 =  (8*pi*r*dr)*(10^6);
fprintf('  Method 3: %15.6f square metres\n', delta_A3)